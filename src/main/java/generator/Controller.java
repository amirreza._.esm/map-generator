package generator;

import generator.models.data.GlobalData;
import generator.models.data.MapScene;
import generator.models.elements.data.Condition;
import generator.models.elements.data.Item;
import generator.models.elements.drawable.Block;
import generator.models.views.list.ConditionsListView;
import generator.models.views.list.ItemListView;
import generator.models.views.popup.ConditionPopup;
import generator.models.views.popup.ItemPopup;
import generator.models.views.popup.TabPopup;
import generator.models.views.popup.WindowedPopup;
import generator.models.views.view.CustomTab;
import generator.models.views.view.DetailsPane;
import generator.models.views.view.ZoomPane;
import generator.utility.LoadUtility;
import generator.utility.SaveUtility;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Point2D;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.control.*;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class Controller {
    public static final double CANVAS_SIZE_X = 1000;
    public static final double CANVAS_SIZE_Y = 1000;


    public static DetailsPane dPane = new DetailsPane();

    private static ConditionsListView globalConditionListView = new ConditionsListView();

    private static ItemListView globalItemListView = new ItemListView();

    public static MapScene currentMapScene = new MapScene(CANVAS_SIZE_X, CANVAS_SIZE_Y);

    private static ZoomPane currentZoomPane;

    public static GlobalData globalData = new GlobalData();

    private static Label errorLabel = new Label();
    private static ScrollPane errorScroller = new ScrollPane();


    private List<MapScene> mapScenes = new ArrayList<>();

    private String nodeToAdd = "block";


    @FXML
    private TabPane mainTabPane;

    @FXML
    private ChoiceBox<String> actionSelector;

    @FXML
    private ToggleButton addBlockBtn;

    @FXML
    private Button saveTabBtn;

    @FXML
    private Button saveGlobalBtn;

    @FXML
    private Button loadTabBtn;

    @FXML
    private Button loadGlobalBtn;

    @FXML
    private ScrollPane detailsPane;

    @FXML
    private Button newTabBtn;

    @FXML
    private Button newConditionBtn;

    @FXML
    private Button newItemBtn;

    @FXML
    private TabPane propertiesTabPane;

    @FXML
    private Button centerViewPortBtn;

    @FXML
    private ToggleButton snapToGridBtn;

    @FXML
    private TitledPane errorLabelHolder;

    @FXML
    public void initialize() {
        setupGlobalData();
        setupUi();
    }


    private void setupUi() {
        errorLabelHolder.setContent(errorScroller);
        errorScroller.setContent(errorLabel);
        errorScroller.setPadding(new Insets(5, 5, 0, 5));
//        errorLabel.setPadding(new Insets(0 , 0, 10,0));
        errorLabel.setTextFill(Color.RED);

        //
        newItemBtn.setOnMouseClicked(event -> {
            ItemPopup itemPopup = new ItemPopup(0, 0,
                    "New item", WindowedPopup.PopupMode.CREATE);
            itemPopup.setPositionRelativeTo(newItemBtn);

            itemPopup.setActionHandler(() -> {
                Item item = itemPopup.updateItem(new Item());
                globalData.getItems().add(item);
                globalItemListView.setList(globalData.getItems());
            });

            itemPopup.show(newItemBtn.getScene().getWindow());
        });

        //

        newConditionBtn.setOnMouseClicked(event -> {
            ConditionPopup newConditionPopup = new ConditionPopup(0, 0,
                    "New condition", WindowedPopup.PopupMode.CREATE);
            newConditionPopup.setPositionRelativeTo(newConditionBtn);

            newConditionPopup.setActionHandler(() -> {
                globalData.getConditions().add(newConditionPopup.updateCondition(new Condition()));
                globalConditionListView.setList(globalData.getConditions());
            });

            newConditionPopup.show(newTabBtn.getScene().getWindow());
        });

        //
        Tab itemsTab = new Tab("Items");
        propertiesTabPane.getTabs().add(itemsTab);
        itemsTab.setContent(globalItemListView);

        //
        Tab conditionsTab = new Tab("Conditions");
        propertiesTabPane.getTabs().add(conditionsTab);
        conditionsTab.setContent(globalConditionListView);

        //
        detailsPane.setContent(dPane);

        //
        newTabBtn.setOnAction(event -> {
            Point2D position = newTabBtn.localToScreen(new Point2D(newTabBtn.getLayoutX(), newTabBtn.getLayoutY() + newTabBtn.getHeight() + 15));

            TabPopup newTabPopup = new TabPopup(position.getX(), position.getY());
            newTabPopup.getCreateBtn().setOnAction(event1 -> {
                createNewTab(newTabPopup.getTabNameTF().getText(), new MapScene(CANVAS_SIZE_X, CANVAS_SIZE_Y));
                newTabPopup.hide();
            });
            newTabPopup.show(newTabBtn.getScene().getWindow());
        });

        //
        newConditionBtn.setOnAction(event -> {

        });

        //
        mainTabPane.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                CustomTab currentTab = ((CustomTab) newValue);
                currentMapScene = currentTab.getMapScene();
                currentZoomPane = currentTab.getZoomPane();
                actionSelector.setValue(currentMapScene.getState().mode);
                snapToGridBtn.setSelected(currentMapScene.getState().snapToGrid);
            }
        });

        //
        centerViewPortBtn.setOnMouseClicked(event -> {
            if (currentZoomPane != null)
                currentZoomPane.centerViewPort();
        });

        //
        actionSelector.getItems().addAll("Move", "Add", "Delete");
        actionSelector.getSelectionModel().select(0);
        actionSelector.setOnAction(event -> {
            currentMapScene.getState().mode = actionSelector.getValue();
            switch (currentMapScene.getState().mode) {
                case "Move":
                    currentMapScene.getCanvas().setCursor(Cursor.DEFAULT);
                    break;
                case "Add":
                    currentMapScene.getCanvas().setCursor(Cursor.CROSSHAIR);
                    break;
                case "Delete":
                    currentMapScene.getCanvas().setCursor(Cursor.DEFAULT);
                    break;
            }
        });

        //
        snapToGridBtn.setOnMouseClicked(event -> {
            currentMapScene.getState().snapToGrid = snapToGridBtn.isSelected();
        });

        //
        saveGlobalBtn.setOnMouseClicked(event -> {
            File defaultSaveAddress = new File("./Scenes/GlobalData/");
            defaultSaveAddress.mkdirs();
            SaveUtility.saveGlobalData(globalData, "./Scenes/GlobalData/" + "global_data.txt");
        });

        //
        loadGlobalBtn.setOnAction(event -> {
            FileChooser fileChooser = new FileChooser();
            File defaultLoadAddress;
            defaultLoadAddress = new File("./Scenes/GlobalData/");
            if (!defaultLoadAddress.isDirectory())
                defaultLoadAddress = new File("./");
            fileChooser.setInitialDirectory(defaultLoadAddress);
            File file = fileChooser.showOpenDialog(loadGlobalBtn.getScene().getWindow());
            if (file != null) {
                try {
                    globalData = LoadUtility.loadGlobalData(file);
                    globalConditionListView.setList(globalData.getConditions());
                    globalItemListView.setList(globalData.getItems());
                } catch (Exception e) {
                    showError("Load procedure", "File corrupted.");
                    e.printStackTrace();
                }
            }

        });

        //
        saveTabBtn.setOnMouseClicked(event -> {
            File defaultSaveAddress = new File("./Scenes/");
            defaultSaveAddress.mkdirs();
            SaveUtility.saveMapScene(currentMapScene, "./Scenes/" + currentMapScene.getTitle() + ".txt");
        });

        //
        loadTabBtn.setOnAction(event -> {
            FileChooser fileChooser = new FileChooser();
            File defaultLoadAddress;
            defaultLoadAddress = new File("./Scenes/");
            if (!defaultLoadAddress.isDirectory())
                defaultLoadAddress = new File("./");
            fileChooser.setInitialDirectory(defaultLoadAddress);
            List<File> files = fileChooser.showOpenMultipleDialog(loadTabBtn.getScene().getWindow());
            if (files != null)
                files.forEach(file -> {
                    MapScene mapScene = LoadUtility.loadMapScene(file);
                    createNewTab(mapScene.getTitle(), mapScene);
                });
        });

        //
        addBlockBtn.setOnMouseClicked(event -> {
            addBlockBtn.setSelected(true);
            nodeToAdd = "block";
            actionSelector.setValue("Add");
        });
    }

    private void setupGlobalData() {
        //Create a new condition list if there isn't any
        if (globalData.getItems() == null)
            globalData.setItems(FXCollections.observableArrayList());

        //Create a new condition list if there isn't any
        if (globalData.getConditions() == null)
            globalData.setConditions(FXCollections.observableArrayList());
    }


    private void createNewTab(String title, MapScene mapScene) {
        CustomTab tab = new CustomTab();

        tab.setText(title);

        mapScene.setTitle(title);
        Controller.currentMapScene = mapScene;
        mapScenes.add(mapScene);
        tab.setMapScene(mapScene);


        final Group group = new Group(
                mapScene.getCanvas()
        );

        mapScene.getCanvas().setOnMouseClicked(event -> {
            if (event.getButton() == MouseButton.PRIMARY) {
                if (mapScene.getState().mode.equals("Add")) {
                    switch (nodeToAdd) {
                        case "block":
                            Block block = new Block(mapScene.getCanvas(), new Point2D(event.getX() - 30, event.getY() - 30), new Point2D(50, 50), "room");
                            mapScene.getCanvas().getChildren().add(block.getShape());
                            currentMapScene.getData().blocks.add(block);
                            dPane.update(block);
                            actionSelector.setValue("Move");
                            break;
                    }
                }
            }
        });

        ZoomPane zoomPane = new ZoomPane(group);
        tab.setZoomPane(zoomPane);
        tab.setContent(zoomPane);
        mainTabPane.getTabs().add(tab);
    }

    public static void showError(String source, String message) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss");

        errorLabel.setText(errorLabel.getText() +
                "\n" +
                simpleDateFormat.format(new Date()) + " - " + source + ": " + message +
                "\n" +
                "=");

        errorScroller.setVvalue(1D);
    }
}
