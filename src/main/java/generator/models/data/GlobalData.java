package generator.models.data;

import generator.models.elements.data.Condition;
import generator.models.elements.data.Item;
import javafx.collections.ObservableList;

public class GlobalData {
    private ObservableList<Condition> conditions;


    private ObservableList<Item> items;

    public ObservableList<Condition> getConditions() {
        return conditions;
    }

    public GlobalData setConditions(ObservableList<Condition> conditions) {
        this.conditions = conditions;
        return this;
    }

    public ObservableList<Item> getItems() {
        return items;
    }

    public void setItems(ObservableList<Item> items) {
        this.items = items;
    }
}
