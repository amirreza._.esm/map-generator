package generator.models.data;

public class IdTextPair {
    Integer id;
    String text;

    public IdTextPair() {
    }

    public IdTextPair(Integer id, String text) {
        this.id = id;
        this.text = text;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
