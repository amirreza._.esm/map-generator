package generator.models.data;

import generator.models.elements.drawable.Block;
import generator.models.elements.drawable.Portal;

import java.util.ArrayList;
import java.util.List;

public class ListContainer {
    public List<Block> blocks = new ArrayList<>();
    public List<Portal> portals = new ArrayList<>();
}
