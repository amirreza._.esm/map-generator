package generator.models.data;

import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;

public class MapScene {
    public static final double CELL_SIZE_X = 50;
    public static final double CELL_SIZE_Y = 50;

    private String title;

    private Pane canvas;

    private State state;

    private ListContainer data;

    public MapScene(double canvasWidth, double canvasHeight) {
        this.canvas = new Pane();
        this.state = new State();
        this.data = new ListContainer();
        drawGridOnCanvas(this.canvas, canvasWidth, canvasHeight, CELL_SIZE_X, CELL_SIZE_Y);
    }

    public String getTitle() {
        return title;
    }

    public MapScene setTitle(String title) {
        this.title = title;
        return this;
    }

    public Pane getCanvas() {
        return canvas;
    }

    public MapScene setCanvas(Pane canvas) {
        this.canvas = canvas;
        return this;
    }

    public State getState() {
        return state;
    }

    public MapScene setState(State state) {
        this.state = state;
        return this;
    }

    public ListContainer getData() {
        return data;
    }

    public MapScene setData(ListContainer data) {
        this.data = data;
        return this;
    }

    private void drawGridOnCanvas(Pane canvas, double width, double height, double cellX, double cellY) {
//        System.out.println("drawing: " + width + " " + height);
        Circle centerCircle = new Circle(width / 2, height / 2, 5, Color.DARKGREY);
        canvas.getChildren().add(centerCircle);
        for (int i = 0; i <= height; i += cellX) {
            Line line = new Line(0, i, width, i);
            line.setStroke(Color.GRAY);
            canvas.getChildren().add(line);
        }
        for (int i = 0; i <= width; i += cellY) {
            Line line = new Line(i, 0, i, height);
            line.setStroke(Color.GRAY);
            canvas.getChildren().add(line);
        }
    }
}
