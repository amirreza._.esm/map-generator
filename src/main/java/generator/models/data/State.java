package generator.models.data;

import generator.models.elements.drawable.PortalEnd;

public class State {
    public String mode = "Move";
    public Double zoomValue = 1.0;
    public boolean snapToGrid = false;

    public boolean isCreatingPortal = false;
    public PortalEnd createPortalEnd;
}
