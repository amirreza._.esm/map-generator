package generator.models.dtos;

public class MapElementDto {
    private int id;
    private String name;
    private String shape;

    public MapElementDto(int id, String name, String shape) {
        this.id = id;
        this.name = name;
        this.shape = shape;
    }

    public int getId() {
        return id;
    }

    public MapElementDto setId(int id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public MapElementDto setName(String name) {
        this.name = name;
        return this;
    }

    public String getShape() {
        return shape;
    }

    public MapElementDto setShape(String shape) {
        this.shape = shape;
        return this;
    }
}
