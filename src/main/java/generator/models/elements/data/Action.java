package generator.models.elements.data;

import com.google.gson.annotations.Expose;
import generator.models.elements.drawable.PortalEnd;
import javafx.geometry.Point2D;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

import java.util.ArrayList;
import java.util.List;

public class Action {
    private static int totalCount = 0;
    private int actionId;

    private void updateID() {
        actionId = totalCount;
        totalCount++;
    }

    private String tag;
    private String Description;

    private String FailMessage;
    private String SuccessMessage;

    private List<Condition> visibilityConditions;
    private List<Condition> doableConditions;
    private List<Condition> changeConditions;

    //Used for loading actions ONLY
    public Action(int actionId) {
        this.actionId = actionId;
    }

    public Action() {
        updateID();
    }

    public int getActionId() {
        return actionId;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getFailMessage() {
        return FailMessage;
    }

    public void setFailMessage(String failMessage) {
        FailMessage = failMessage;
    }

    public String getSuccessMessage() {
        return SuccessMessage;
    }

    public void setSuccessMessage(String successMessage) {
        SuccessMessage = successMessage;
    }

    public List<Condition> getVisibilityConditions() {
        return visibilityConditions;
    }

    public void setVisibilityConditions(List<Condition> visibilityConditions) {
        this.visibilityConditions = visibilityConditions;
    }

    public List<Condition> getDoableConditions() {
        return doableConditions;
    }

    public void setDoableConditions(List<Condition> doableConditions) {
        this.doableConditions = doableConditions;
    }

    public List<Condition> getChangeConditions() {
        return changeConditions;
    }

    public void setChangeConditions(List<Condition> changeConditions) {
        this.changeConditions = changeConditions;
    }
}
