package generator.models.elements.data;

public class Condition {
    private static int totalCount = 0;
    private int conditionId;

    private void updateID() {
        conditionId = totalCount;
        totalCount++;
    }

    private String key;
    private String value;

    //Load ONLY!
    public Condition(int conditionId) {
        this.conditionId = conditionId;
    }

    public Condition() {
        updateID();
    }

    public Condition(int id, String key, String value) {
        //Used only when adding an condition instance to a data
        this.conditionId = id;
        this.key = key;
        this.value = value;
    }

    public Condition(String key, String value) {
        updateID();
        this.key = key;
        this.value = value;
    }

    public int getConditionId() {
        return conditionId;
    }

    public String getKey() {
        return key;
    }

    public Condition setKey(String key) {
        this.key = key;
        return this;
    }

    public String getValue() {
        return value;
    }

    public Condition setValue(String value) {
        this.value = value;
        return this;
    }
}
