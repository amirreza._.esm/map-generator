package generator.models.elements.data;

import java.util.List;

public class Item {
    private static int totalCount = 0;
    private int itemId;

    private void updateID() {
        itemId = totalCount;
        totalCount++;
    }


    private String name;

    private String description;

    private List<Condition> visibilityConditions;

    private List<Action> actions;

    //Used to load items ONLY
    public Item(int itemId) {
        this.itemId = itemId;
    }

    public Item() {
        updateID();
    }

    public int getItemId() {
        return itemId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Condition> getVisibilityConditions() {
        return visibilityConditions;
    }

    public void setVisibilityConditions(List<Condition> visibilityConditions) {
        this.visibilityConditions = visibilityConditions;
    }

    public List<Action> getActions() {
        return actions;
    }

    public void setActions(List<Action> actions) {
        this.actions = actions;
    }
}
