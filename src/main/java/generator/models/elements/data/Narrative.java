package generator.models.elements.data;

import java.util.List;

public class Narrative {
    private static int totalCount = 0;
    private int narrativeId;

    private void updateID() {
        narrativeId = totalCount;
        totalCount++;
    }

    private String tag;
    private String massage;
    private List<Condition> conditions;

    //Load ONLY!
    public Narrative(int narrativeId) {
        this.narrativeId = narrativeId;
    }

    public Narrative() {
        updateID();
    }

    public Narrative(String tag, String massage, List<Condition> conditions) {
        updateID();
        this.tag = tag;
        this.massage = massage;
        this.conditions = conditions;
    }

    public int getNarrativeId() {
        return narrativeId;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getMassage() {
        return massage;
    }

    public Narrative setMassage(String massage) {
        this.massage = massage;
        return this;
    }

    public List<Condition> getConditions() {
        return conditions;
    }

    public Narrative setConditions(List<Condition> conditions) {
        this.conditions = conditions;
        return this;
    }
}
