package generator.models.elements.drawable;

import com.google.gson.annotations.Expose;
import generator.Controller;
import generator.models.elements.data.Narrative;
import generator.models.data.MapScene;
import javafx.geometry.Point2D;
import javafx.scene.Cursor;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.StrokeType;

import java.util.ArrayList;
import java.util.List;

public class Block extends MapElement {

/*
    The parent pane is created in updateShape function.
 */

    public final static double UNIT_SIZE_X = 50;
    public final static double UNIT_SIZE_y = 50;

    private static int totalCount = 0;
    @Expose
    private int blockId;
    @Expose
    private Point2D size;
    private Color texture;
    @Expose
    private String type;
    @Expose
    private List<PortalEnd> portalEnds = new ArrayList<>();

    private List<Narrative> narratives = new ArrayList<>();

    private List<Integer> itemIds = new ArrayList<>();

    //ui
    private Rectangle rect;

    private Color mainColor;
    private Color selectedColor;
    private Color deleteColor;

    //assistive values:
    private boolean isSelected;
    private double pressedX;
    private double pressedY;
    private double nodeX;
    private double nodeY;

    private void updateID() {
        blockId = totalCount;
        totalCount++;
    }

    public Block(Pane canvas, Point2D position, Point2D size, String type) {
        super(canvas);
        this.size = size;
        this.position = position;
        this.type = type;

        //Define colors
        mainColor = Color.DARKKHAKI;
        selectedColor = Color.YELLOWGREEN;
        deleteColor = Color.LIGHTCORAL;

        Pane pane = new Pane();
        this.setShape(pane);
        this.rect = blockFactory(pane, size.getX(), size.getY(), blockId);
        pane.getChildren().add(rect);
        updateID();
        updateShape();
    }

    public int getBlockId() {
        return blockId;
    }

    //Used in loading a new scene
    public Block setBlockId(int blockId) {
        this.blockId = blockId;
        return this;
    }

    public Point2D getSize() {
        return size;
    }

    public Block setSize(Point2D size) {
        this.size = size;
        return this;
    }

    public Point2D getSizeInUnit() {
        return new Point2D(size.getX() / UNIT_SIZE_X, size.getY() / UNIT_SIZE_y);
    }

    public void setSizeInUnit(Point2D size) {
        this.size = new Point2D(size.getX() * UNIT_SIZE_X, size.getY() * UNIT_SIZE_y);
    }

    public Color getTexture() {
        return texture;
    }

    public Block setTexture(Color texture) {
        this.texture = texture;
        return this;
    }

    public String getType() {
        return type;
    }

    public Block setType(String type) {
        this.type = type;
        return this;
    }

    public List<PortalEnd> getPortalEnds() {
        return portalEnds;
    }

    //Used in loading a new scene
    public Block setPortalEnds(List<PortalEnd> portalEnds) {
        this.portalEnds = portalEnds;
        return this;
    }

    public List<Narrative> getNarratives() {
        return narratives;
    }

    public Block setNarratives(List<Narrative> narratives) {
        this.narratives = narratives;
        return this;
    }

    public List<Integer> getItemIds() {
        return itemIds;
    }

    public void setItemIds(List<Integer> itemIds) {
        this.itemIds = itemIds;
    }

    public void updateShape() {

        Pane pane = (Pane) getShape();
        pane.relocate(position.getX(), position.getY());
        pane.setPrefWidth(size.getX());
        pane.setPrefHeight(size.getY());

        rect.setWidth(size.getX());
        rect.setHeight(size.getY());
        updatePortalEnds();
    }


    public void createPortalEnd(int side) {
        PortalEnd portalEnd = new PortalEnd(canvas, 0, 0, getBlockId(), side);
        portalEnds.add(portalEnd);
        ((Pane) shape).getChildren().add(portalEnd.getShape());
        updatePortalEnds();
    }

    public void updatePortalEnds() {
        //Update portal ends
        for (int side = 0; side < 4; side++) {
            double x = 0;
            double y = 0;

            int thisSideEnds = 0;
            for (PortalEnd pe : portalEnds) {
                if (pe.getSide() == side)
                    thisSideEnds += 1;
            }

            int i = 0;
            for (PortalEnd pe : portalEnds) {
                if (pe.getSide() == side && i < thisSideEnds) {
                    switch (side) {
                        case 0:
                            x = (i * (size.getX() / thisSideEnds)) + ((size.getX() / thisSideEnds) / 2 + 0) - 5;
                            y = 0 - 5;
                            break;
                        case 1:
                            x = size.getX() - 5;
                            y = (i * (size.getY() / thisSideEnds)) + ((size.getY() / thisSideEnds) / 2 + 0) - 5;
                            break;
                        case 2:
                            x = (i * (size.getX() / thisSideEnds)) + ((size.getX() / thisSideEnds) / 2 + 0) - 5;
                            y = size.getY() - 5;
                            break;
                        case 3:
                            x = 0 - 5;
                            y = (i * (size.getY() / thisSideEnds)) + ((size.getY() / thisSideEnds) / 2 + 0) - 5;
                            break;
                    }
                    pe.setPosition(new Point2D(x, y));
                    pe.relocate(x, y);
                    i++;
                }
            }
        }

        //Update portals
        for (Portal portal : Controller.currentMapScene.getData().portals) {
            if (portal.getStart().getOwnerBlockId() == getBlockId() || portal.getEnd().getOwnerBlockId() == getBlockId())
                portal.updatePosition();
        }
    }


    private Rectangle blockFactory(Pane pane, double x, double y, int blockId) {
        Rectangle rect = new Rectangle();
        rect.setFill(mainColor);
        rect.setStroke(Color.BROWN);
        rect.setStrokeWidth(2);
        rect.setStrokeType(StrokeType.INSIDE);
        rect.setHeight(y);
        rect.setWidth(x);
        rect.setArcHeight(15);
        rect.setArcWidth(15);
        rect.relocate(0, 0);


        rect.setOnMouseEntered(event -> {
            if (Controller.currentMapScene.getState().mode.equals("Delete")) {
                rect.setFill(deleteColor);
            }
            if (Controller.currentMapScene.getState().mode.equals("Move")) {
                pane.setCursor(Cursor.MOVE);
            }
        });

        rect.setOnMouseExited(event -> {
            if (isSelected)
                rect.setFill(selectedColor);
            else
                rect.setFill(mainColor);

            if (Controller.currentMapScene.getState().mode.equals("Move")) {
                pane.setCursor(Cursor.DEFAULT);
            }
        });

        rect.setOnMouseClicked(event -> {
            if (Controller.currentMapScene.getState().mode.equals("Delete")) {
                Controller.currentMapScene.getData().blocks.remove(this);
                canvas.getChildren().remove(this.getShape());
                Controller.dPane.update(null);

                //Delete portals
                List<Portal> toDelete = new ArrayList<>();
                for (Portal portal : Controller.currentMapScene.getData().portals) {
                    if (portal.getStart().getOwnerBlockId() == getBlockId()
                            || portal.getEnd().getOwnerBlockId() == getBlockId()) {
                        toDelete.add(portal);
                    }
                }

                for (Portal portal : toDelete) {
                    Controller.currentMapScene.getData().portals.remove(portal);
                    canvas.getChildren().remove(portal.getShape());
                }
            }
        });


        rect.setOnMousePressed(event -> {
            if (Controller.currentMapScene.getState().mode.equals("Move")) {
                pressedX = event.getSceneX();
                pressedY = event.getSceneY();
                nodeX = pane.getLayoutX();
                nodeY = pane.getLayoutY();
                Controller.dPane.update(this);
            }
        });
        rect.setOnMouseDragged(event -> {
            if (Controller.currentMapScene.getState().mode.equals("Move")) {
//                double centerX = pane.getBoundsInParent().getWidth() / 2;
//                double centerY = pane.getBoundsInParent().getHeight() / 2;

                double deltaX = nodeX + (event.getSceneX() - pressedX) * (1 / Controller.currentMapScene.getState().zoomValue);
                double deltaY = nodeY + (event.getSceneY() - pressedY) * (1 / Controller.currentMapScene.getState().zoomValue);

                if (Controller.currentMapScene.getState().snapToGrid) {
                    double snappedDeltaX = deltaX;
                    double snappedDeltaY = deltaY;

                    if(deltaX % MapScene.CELL_SIZE_X < 5)
                        snappedDeltaX = deltaX - deltaX % MapScene.CELL_SIZE_X;
                    if(deltaX % MapScene.CELL_SIZE_X > MapScene.CELL_SIZE_X - 5)
                        snappedDeltaX = deltaX +(MapScene.CELL_SIZE_X - deltaX % MapScene.CELL_SIZE_X);

                    if(deltaY % MapScene.CELL_SIZE_Y < 5)
                        snappedDeltaY = deltaY - deltaY % MapScene.CELL_SIZE_Y;
                    if(deltaY % MapScene.CELL_SIZE_Y > MapScene.CELL_SIZE_Y - 5)
                        snappedDeltaY = deltaY +(MapScene.CELL_SIZE_Y - deltaY % MapScene.CELL_SIZE_Y);

                    this.relocate(snappedDeltaX, snappedDeltaY);
                }
                else
                    this.relocate(deltaX, deltaY);
                //Update detail pane
                Controller.dPane.update(this);
                //Update portals
                for (Portal portal : Controller.currentMapScene.getData().portals) {
                    if (portal.getStart().getOwnerBlockId() == getBlockId() || portal.getEnd().getOwnerBlockId() == getBlockId())
                        portal.updatePosition();
                }
                event.consume();
            }
        });
        return rect;
    }

    public void setSelected(boolean selected) {
        if (selected) {
            isSelected = true;
            this.rect.setFill(selectedColor);
        } else {
            this.rect.setFill(mainColor);
            isSelected = false;
        }
    }
}
