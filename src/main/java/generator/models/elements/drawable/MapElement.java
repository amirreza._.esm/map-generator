package generator.models.elements.drawable;

import com.google.gson.annotations.Expose;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.layout.Pane;

public class MapElement {
    private static int count = 0;
    private int globalId;
    private void updateID() {
        globalId = count;
        count++;
    }

    @Expose
    Point2D position;
    Node shape;
    Pane canvas;

    public MapElement(Pane canvas) {
        this.canvas = canvas;
        updateID();
    }



    public static int getCount() {
        return count;
    }

    public int getId() {
        return globalId;
    }


    public Point2D getPosition() {
        return position;
    }

    public MapElement setPosition(Point2D position) {
        this.position = position;
        return this;
    }

    public Node getShape() {
        return shape;
    }

    public MapElement setShape(Node shape) {
        this.shape = shape;
        return this;
    }

    public void relocate(double X, double Y) {
        this.position = new Point2D(X + 0, Y + 0);
        shape.relocate(X, Y);
    }
}
