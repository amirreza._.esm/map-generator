package generator.models.elements.drawable;

import javafx.geometry.Point2D;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;

public class Portal extends MapElement {

    private static int totalCount = 0;
    private int portalId;

    public int startBlockId;
    public int startSide;
    public int endBlockId;
    public int endSide;

    private PortalEnd start;

    public double startX = 0;
    public double startY = 0;
    public double endX = 0;
    public double endY = 0;

    private PortalEnd end;

    private void updateID() {
        portalId = totalCount;
        totalCount++;
    }

    public Portal(Pane canvas) {
        super(canvas);
        updateID();
    }

    public Portal(Pane canvas, PortalEnd start, PortalEnd end) {
        super(canvas);
        updateID();
        this.start = start;
        this.end = end;

        this.startBlockId = start.getOwnerBlockId();
        this.startSide = start.getSide();
        this.endBlockId = end.getOwnerBlockId();
        this.endSide = end.getSide();

        Line line = new Line(0, 0, 0, 0);
        line.setStroke(Color.DARKGREEN);
        line.setStrokeWidth(3);
        this.setShape(line);
        updatePosition();
    }


    public int getPortalId() {
        return portalId;
    }


    //Used in loading scene
    public Portal setPortalId(int portalId) {
        this.portalId = portalId;
        return this;
    }

    public PortalEnd getStart() {
        return start;
    }

    public PortalEnd getEnd() {
        return end;
    }

    public double getStartX() {
        return startX;
    }

    public Portal setStartX(double startX) {
        this.startX = startX;
        return this;
    }

    public double getStartY() {
        return startY;
    }

    public Portal setStartY(double startY) {
        this.startY = startY;
        return this;
    }

    public double getEndX() {
        return endX;
    }

    public Portal setEndX(double endX) {
        this.endX = endX;
        return this;
    }

    public double getEndY() {
        return endY;
    }

    public Portal setEndY(double endY) {
        this.endY = endY;
        return this;
    }

    public void updatePosition(){
        Point2D offset = new Point2D(0, 0);
        this.startX = canvas.sceneToLocal(start.getShape().localToScene(offset)).getX();
        this.startY = canvas.sceneToLocal(start.getShape().localToScene(offset)).getY();

        this.endX = canvas.sceneToLocal(end.getShape().localToScene(offset)).getX();
        this.endY = canvas.sceneToLocal(end.getShape().localToScene(offset)).getY();

        Line line = (Line) getShape();
        line.setStartX(this.startX);
        line.setStartY(this.startY);
        line.setEndX(this.endX);
        line.setEndY(this.endY);
    }

}
