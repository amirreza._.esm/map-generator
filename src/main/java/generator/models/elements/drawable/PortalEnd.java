package generator.models.elements.drawable;

import generator.Controller;
import javafx.application.Platform;
import javafx.geometry.Point2D;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

import java.util.ArrayList;
import java.util.List;

/*
The parent pane is created in constructor.
 */

public class PortalEnd extends MapElement {

    private static int totalCount = 0;
    private int portalEndId;

    private int ownerBlockId;
    private int side;

    private void updateID() {
        portalEndId = totalCount;
        totalCount++;
    }

    public int getPortalEndId() {
        return portalEndId;
    }

    //Used for loading scene
    public PortalEnd setPortalEndId(int portalEndId) {
        this.portalEndId = portalEndId;
        return this;
    }

    public PortalEnd(Pane canvas, double x, double y, int blockId, Integer side) {
        super(canvas);
        updateID();
        this.position = new Point2D(x, y);
        this.ownerBlockId = blockId;
        this.side = side;
        Circle circle = circleFactory(0, 0, blockId, side);
        this.setShape(circle);
        this.relocate(x, y);
    }

    public int getOwnerBlockId() {
        return ownerBlockId;
    }

    public int getSide() {
        return side;
    }

    Circle circleFactory(double x, double y, int blockId, Integer side) {
        Circle circle = new Circle(x, y, 5);
        circle.setUserData(side);
        circle.setFill(Color.GREEN);
        circle.setStroke(Color.BLACK);
        circle.setOnMouseEntered(event1 -> {
            if (Controller.currentMapScene.getState().mode.equals("Move"))
                circle.setFill(Color.GREENYELLOW);
            if (Controller.currentMapScene.getState().mode.equals("Delete"))
                circle.setFill(Color.PALEVIOLETRED);
        });
        circle.setOnMouseExited(event1 -> {
            circle.setFill(Color.GREEN);
        });

        circle.setOnMouseClicked(event -> {
            if (Controller.currentMapScene.getState().mode.equals("Move")) {
                if (!Controller.currentMapScene.getState().isCreatingPortal) {
                    Controller.currentMapScene.getState().isCreatingPortal = true;
                    Controller.currentMapScene.getState().createPortalEnd = this;
                    System.out.println("start id: " + Controller.currentMapScene.getState().createPortalEnd.getOwnerBlockId() + " side: " + Controller.currentMapScene.getState().createPortalEnd.getSide());
                } else {
                    System.out.println("end id: " + blockId + " side: " + side);
                    Controller.currentMapScene.getState().isCreatingPortal = false;

                    Portal portal = new Portal(canvas, Controller.currentMapScene.getState().createPortalEnd, this);
                    canvas.getChildren().add(portal.getShape());
                    Controller.currentMapScene.getData().portals.add(portal);
                }
            }
            if (Controller.currentMapScene.getState().mode.equals("Delete")) {
                Platform.runLater(() -> {
                    for (Block block : Controller.currentMapScene.getData().blocks) {
                        if (block.getBlockId() == getOwnerBlockId()) {
                            for (PortalEnd portalEnd : block.getPortalEnds()) {
                                if (portalEnd.getSide() == getSide() && portalEnd.getPortalEndId() == getPortalEndId())
                                    ((Pane) block.getShape()).getChildren().remove(portalEnd.getShape());
                            }
                            block.getPortalEnds().remove(this);
                            block.updatePortalEnds();
                        }
                    }

                    //Delete portals
                    List<Portal> toDelete = new ArrayList<>();
                    for (Portal portal : Controller.currentMapScene.getData().portals) {
                        if ((portal.getStart().getOwnerBlockId() == getOwnerBlockId() && portal.getStart().getSide() == getSide() && portal.getStart().getPortalEndId() == getPortalEndId())
                                || (portal.getEnd().getOwnerBlockId() == getOwnerBlockId() && portal.getEnd().getSide() == getSide() && portal.getEnd().getPortalEndId() == getPortalEndId())) {
                            toDelete.add(portal);
                        }
                    }

                    for (Portal portal : toDelete) {
                        Controller.currentMapScene.getData().portals.remove(portal);
                        canvas.getChildren().remove(portal.getShape());
                    }
                });
            }

        });
        return circle;
    }
}
