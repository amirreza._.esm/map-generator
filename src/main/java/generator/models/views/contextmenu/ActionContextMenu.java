package generator.models.views.contextmenu;

import generator.models.elements.data.Action;
import generator.models.views.list.ActionListView;
import generator.models.views.popup.ActionPopup;
import generator.models.views.popup.WindowedPopup;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.stage.Screen;

import java.util.List;

public class ActionContextMenu extends ContextMenu {

    public ActionContextMenu(int actionId, List<Action> actionList, ActionListView listView) {
        setAutoHide(true);
        MenuItem editItem = new MenuItem("Edit");
        MenuItem deleteItem = new MenuItem("Delete");

        this.getItems().addAll(editItem, deleteItem);

        Action tempAction = null;
        for (Action c : actionList) {
            if (c.getActionId() == actionId)
                tempAction = c;
        }
        if (tempAction != null) {
            final Action action = tempAction;

            editItem.setOnAction(event -> {
                ActionPopup actionPopup = new ActionPopup(
                        Screen.getPrimary().getBounds().getMaxX() / 2,
                        Screen.getPrimary().getBounds().getMaxY() / 2,
                        "Edit action",
                        WindowedPopup.PopupMode.EDIT);

                actionPopup.setActionHandler(() -> {
                    actionPopup.updateAction(action);
                    listView.update();
                });

                actionPopup.populate(action);
                actionPopup.show(listView.getScene().getWindow());
            });

            deleteItem.setOnAction(event -> {
                actionList.remove(action);
                listView.update();
            });
        } else
            this.hide();
    }
}
