package generator.models.views.contextmenu;

import generator.models.elements.data.Condition;
import generator.models.views.list.ConditionsListView;
import generator.models.views.popup.ConditionPopup;
import generator.models.views.popup.WindowedPopup;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.stage.Screen;

import java.util.List;

public class ConditionContextMenu extends ContextMenu {

    public ConditionContextMenu(int conditionId, List<Condition> conditionList, ConditionsListView listView) {
        setAutoHide(true);
        MenuItem editItem = new MenuItem("Edit");
        MenuItem deleteItem = new MenuItem("Delete");

        this.getItems().addAll(editItem, deleteItem);

        Condition tempCondition = null;
        for (Condition c : conditionList) {
            if (c.getConditionId() == conditionId)
                tempCondition = c;
        }
        if (tempCondition != null) {
            final Condition condition = tempCondition;

            editItem.setOnAction(event -> {
                ConditionPopup conditionPopup = new ConditionPopup(
                        Screen.getPrimary().getBounds().getMaxX() / 2,
                        Screen.getPrimary().getBounds().getMaxY() / 2,
                        "Edit condition",
                        WindowedPopup.PopupMode.EDIT);

                conditionPopup.setActionHandler(() -> {
                    conditionPopup.updateCondition(condition);
                    listView.update();
                });

                conditionPopup.populate(condition);
                conditionPopup.show(listView.getScene().getWindow());
            });

            deleteItem.setOnAction(event -> {
                conditionList.remove(condition);
                listView.update();
            });
        } else
            this.hide();
    }
}
