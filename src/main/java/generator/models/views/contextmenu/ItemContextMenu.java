package generator.models.views.contextmenu;

import generator.models.elements.data.Item;
import generator.models.views.list.ItemListView;
import generator.models.views.popup.ItemPopup;
import generator.models.views.popup.WindowedPopup;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.stage.Screen;

import java.util.List;

public class ItemContextMenu extends ContextMenu {

    public ItemContextMenu(int itemId, List<Item> itemList, ItemListView listView) {
        setAutoHide(true);
        MenuItem editItem = new MenuItem("Edit");
        MenuItem deleteItem = new MenuItem("Delete");

        this.getItems().addAll(editItem, deleteItem);

        Item tempItem = null;
        for (Item item : itemList) {
            if (item.getItemId() == itemId)
                tempItem = item;
        }
        if (tempItem != null) {
            final Item item = tempItem;

            editItem.setOnAction(event -> {
                ItemPopup itemPopup = new ItemPopup(
                        Screen.getPrimary().getBounds().getMaxX() / 2,
                        Screen.getPrimary().getBounds().getMaxY() / 2,
                        "Edit item",
                        WindowedPopup.PopupMode.EDIT);

                itemPopup.setActionHandler(() -> {
                    itemPopup.updateItem(item);
                    listView.update();
                });

                itemPopup.populate(item);
                itemPopup.show(listView.getScene().getWindow());
            });

            deleteItem.setOnAction(event -> {
                itemList.remove(item);
                listView.update();
            });
        } else
            this.hide();
    }
}
