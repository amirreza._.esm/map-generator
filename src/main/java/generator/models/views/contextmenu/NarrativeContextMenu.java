package generator.models.views.contextmenu;

import generator.models.elements.data.Narrative;
import generator.models.views.list.NarrativeListView;
import generator.models.views.popup.NarrativePopup;
import generator.models.views.popup.WindowedPopup;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.stage.Screen;

import java.util.List;

public class NarrativeContextMenu extends ContextMenu {

    public NarrativeContextMenu(int narrativeId, List<Narrative> narrativeList, NarrativeListView listView) {
        setAutoHide(true);
        MenuItem editItem = new MenuItem("Edit");
        MenuItem deleteItem = new MenuItem("Delete");

        this.getItems().addAll(editItem, deleteItem);

        Narrative tempNarrative = null;
        for (Narrative c : narrativeList) {
            if (c.getNarrativeId() == narrativeId)
                tempNarrative = c;
        }
        if (tempNarrative != null) {
            final Narrative narrative = tempNarrative;

            editItem.setOnAction(event -> {
                NarrativePopup narrativePopup = new NarrativePopup(
                        Screen.getPrimary().getBounds().getMaxX() / 2,
                        Screen.getPrimary().getBounds().getMaxY() / 2,
                        "Edit narrative",
                        WindowedPopup.PopupMode.EDIT);

                narrativePopup.setActionHandler(() -> {
                    narrativePopup.updateNarrative(narrative);
                    listView.update();
                });

                narrativePopup.populate(narrative);
                narrativePopup.show(listView.getScene().getWindow());
            });

            deleteItem.setOnAction(event -> {
                narrativeList.remove(narrative);
                listView.update();
            });
        } else
            this.hide();
    }
}
