package generator.models.views.list;

import generator.models.data.IdTextPair;
import generator.models.elements.data.Action;
import generator.models.views.contextmenu.ActionContextMenu;
import javafx.scene.control.ListView;

import java.util.List;
import java.util.stream.Collectors;

public class ActionListView extends ListView<IdTextPair> {

    private List<Action> actionList;

    public ActionListView() {

        this.setCellFactory(param -> new IdTextListCell());


        this.setOnContextMenuRequested(event -> {
            int actionId = this.getSelectionModel().getSelectedItem().getId();

            ActionContextMenu contextMenu = new ActionContextMenu(actionId, actionList, this);
            contextMenu.show(this, event.getScreenX(), event.getScreenY());
        });
    }

    public void setList(List<Action> conditions) {
        this.actionList = conditions;
        update();
    }

    public void update() {
        this.getItems().setAll(actionList.stream().map(c -> new IdTextPair(c.getActionId(), c.getTag())).collect(Collectors.toList()));
    }

}
