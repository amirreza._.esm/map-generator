package generator.models.views.list;

import generator.models.elements.data.Condition;
import javafx.scene.control.ListCell;
import javafx.stage.Popup;

public class ConditionListCell extends ListCell<Condition> {
    Popup detailsPopup;
    Condition condition = null;

    public ConditionListCell() {
//        this.setOnMouseEntered(event -> {
//            if(condition != null) {
//                detailsPopup = new Popup();
//                detailsPopup.setX(event.getScreenX());
//                detailsPopup.setY(event.getScreenY());
//                detailsPopup.getContent().add(new Label(condition.getValue() + ""));
//                detailsPopup.show(this.getScene().getWindow());
//            }
//        });
//
//        this.setOnMouseExited(event -> {
//            if (detailsPopup != null)
//                detailsPopup.hide();
//            detailsPopup = null;
//        });
    }

    @Override
    protected void updateItem(Condition item, boolean empty) {
        super.updateItem(item, empty);
        if (item != null) {
            this.setText(item.getKey() + " : " + item.getValue());
            this.condition = item;
        }
    }
}
