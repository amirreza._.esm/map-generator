package generator.models.views.list;

import generator.models.data.IdTextPair;
import generator.models.elements.data.Condition;
import generator.models.views.contextmenu.ConditionContextMenu;
import javafx.scene.control.ListView;

import java.util.List;
import java.util.stream.Collectors;

public class ConditionsListView extends ListView<IdTextPair> {

    private List<Condition> conditionList;

    public ConditionsListView() {

        this.setCellFactory(param -> new IdTextListCell());


        this.setOnContextMenuRequested(event -> {
            int conditionId = this.getSelectionModel().getSelectedItem().getId();

            ConditionContextMenu contextMenu = new ConditionContextMenu(conditionId, conditionList, this);
            contextMenu.show(this, event.getScreenX(), event.getScreenY());
        });
    }

    public void setList(List<Condition> conditions) {
        this.conditionList = conditions;
        update();
    }

    public void update() {
        this.getItems().setAll(conditionList.stream().map(c -> new IdTextPair(c.getConditionId(), c.getKey())).collect(Collectors.toList()));
    }

}
