package generator.models.views.list;

import generator.models.elements.drawable.MapElement;
import javafx.scene.control.ListCell;

public class CustomListCell extends ListCell<MapElement> {

    @Override
    protected void updateItem(MapElement item, boolean empty) {
        super.updateItem(item, empty);
        if (item != null) {
            double centerX = item.getShape().getBoundsInParent().getMinX() + item.getShape().getBoundsInParent().getWidth() / 2;
            double centerY = item.getShape().getBoundsInParent().getMinY() + item.getShape().getBoundsInParent().getHeight() / 2;
            setText("id: " +item.getId()+" x: " + centerX + " y: " + centerY);
        }
    }
}
