package generator.models.views.list;

import generator.models.data.IdTextPair;
import javafx.scene.control.ListCell;

public class IdTextListCell extends ListCell<IdTextPair> {

    @Override
    protected void updateItem(IdTextPair item, boolean empty) {
        super.updateItem(item, empty);
        if (!empty)
            this.setText(item.getText());
    }
}
