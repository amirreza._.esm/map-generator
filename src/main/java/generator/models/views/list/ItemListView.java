package generator.models.views.list;

import generator.models.data.IdTextPair;
import generator.models.elements.data.Item;
import generator.models.views.contextmenu.ItemContextMenu;
import javafx.scene.control.ListView;

import java.util.List;
import java.util.stream.Collectors;

public class ItemListView extends ListView<IdTextPair> {

    private List<Item> itemList;

    public ItemListView() {

        this.setCellFactory(param -> new IdTextListCell());


        this.setOnContextMenuRequested(event -> {
            int itemId = this.getSelectionModel().getSelectedItem().getId();

            ItemContextMenu contextMenu = new ItemContextMenu(itemId, itemList, this);
            contextMenu.show(this, event.getScreenX(), event.getScreenY());
        });
    }

    public void setList(List<Item> items) {
        this.itemList = items;
        update();
    }

    public void update() {
        this.getItems().setAll(itemList.stream().map(c -> new IdTextPair(c.getItemId(), c.getName())).collect(Collectors.toList()));
    }

}