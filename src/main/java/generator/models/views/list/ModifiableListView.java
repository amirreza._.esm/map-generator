package generator.models.views.list;

import generator.models.data.IdTextPair;
import javafx.scene.control.ListView;

public class ModifiableListView extends ListView<IdTextPair> {

    public ModifiableListView() {
        setCellFactory(param -> new IdTextListCell());
    }
}
