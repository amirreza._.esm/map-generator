package generator.models.views.list;

import generator.models.data.IdTextPair;
import generator.models.elements.data.Narrative;
import generator.models.views.contextmenu.ConditionContextMenu;
import generator.models.views.contextmenu.NarrativeContextMenu;
import javafx.scene.control.ListView;

import java.util.List;
import java.util.stream.Collectors;

public class NarrativeListView extends ListView<IdTextPair> {

    private List<Narrative> narrativeList;

    public NarrativeListView() {

        this.setCellFactory(param -> new IdTextListCell());


        this.setOnContextMenuRequested(event -> {
            int narrativeId = this.getSelectionModel().getSelectedItem().getId();

            NarrativeContextMenu contextMenu = new NarrativeContextMenu(narrativeId, narrativeList, this);
            contextMenu.show(this, event.getScreenX(), event.getScreenY());
        });
    }

    public void setList(List<Narrative> conditions) {
        this.narrativeList = conditions;
        update();
    }

    public void update() {
        this.getItems().setAll(narrativeList.stream().map(c -> new IdTextPair(c.getNarrativeId(), c.getTag())).collect(Collectors.toList()));
    }

}
