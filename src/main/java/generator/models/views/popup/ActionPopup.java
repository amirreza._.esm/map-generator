package generator.models.views.popup;

import generator.Controller;
import generator.models.elements.data.Action;
import generator.models.views.view.ConditionPairHolder;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class ActionPopup extends WindowedPopup {

    private HBox hBox = new HBox();
    private TextField tagTf = new TextField("Tag");
    private VBox descriptionVbox = new VBox();
    private TextArea descriptionTf = new TextArea("d");
    private TextArea successTf = new TextArea("success");
    private TextArea failTf = new TextArea("fail");

    private ConditionPairHolder vcHolder = new ConditionPairHolder("Visibility conditions");
    private ConditionPairHolder dcHolder = new ConditionPairHolder("doablity! conditions");
    private ConditionPairHolder ccHolder = new ConditionPairHolder("Change conditions to");

    private Button actionBtn = new Button();

    private FormActionBtnHandler actionHandler;

    public ActionPopup(double x, double y, String titleText, PopupMode mode) {
        super(x, y, titleText);
        hBox.setSpacing(5);

        descriptionVbox.setPrefWidth(400);
        descriptionVbox.setPrefHeight(275);

        switch (mode) {
            case CREATE:
                actionBtn.setText("Create");
                break;
            case EDIT:
                actionBtn.setText("Update");
                break;
        }

        actionBtn.setOnMouseClicked(event -> {
            actionHandler.handle();
            this.hide();
        });

        descriptionVbox.getChildren().addAll(descriptionTf, successTf, failTf);

        hBox.getChildren().addAll(tagTf, descriptionVbox,
                vcHolder, dcHolder, ccHolder,
                actionBtn);

        setMainContent(hBox);
    }

    public Action updateAction(Action action) {
        action.setTag(tagTf.getText());
        action.setDescription(descriptionTf.getText());
        action.setSuccessMessage(successTf.getText());
        action.setFailMessage(failTf.getText());

        action.setVisibilityConditions(vcHolder.getConditions());
        action.setDoableConditions(dcHolder.getConditions());
        action.setChangeConditions(ccHolder.getConditions());

        return action;
    }

    public void populate(Action action){
        tagTf.setText(action.getTag());
        descriptionTf.setText(action.getDescription());
        successTf.setText(action.getSuccessMessage());
        failTf.setText(action.getFailMessage());

        action.getVisibilityConditions().forEach(condition ->
                vcHolder.addConditionPair(Controller.globalData.getConditions(), condition));
        action.getDoableConditions().forEach(condition ->
                dcHolder.addConditionPair(Controller.globalData.getConditions(), condition));
        action.getChangeConditions().forEach(condition ->
                ccHolder.addConditionPair(Controller.globalData.getConditions(), condition));
    }

    public void setActionHandler(FormActionBtnHandler actionHandler) {
        this.actionHandler = actionHandler;
    }
}
