package generator.models.views.popup;

import generator.models.data.IdTextPair;
import generator.models.elements.data.Item;
import generator.models.views.list.IdTextListCell;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.layout.HBox;

import java.util.List;

public class AddItemPopup extends WindowedPopup {

    private HBox hBox = new HBox();

    private ListView<IdTextPair> itemsListView = new ListView<>();

    private Button addBtn = new Button("Add");

    public AddItemPopup(double x, double y, String titleText) {
        super(x, y, titleText);

        itemsListView.setCellFactory(param -> new IdTextListCell());

        itemsListView.setEditable(true);
        itemsListView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        hBox.getChildren().addAll(itemsListView, addBtn);

        setMainContent(hBox);
    }

    public void setList(List<Item> items) {
        items.forEach(item -> itemsListView.getItems().add(new IdTextPair(item.getItemId(), item.getName())));
    }

    public ListView<IdTextPair> getItemsListView() {
        return itemsListView;
    }

    public Button getAddBtn() {
        return addBtn;
    }
}
