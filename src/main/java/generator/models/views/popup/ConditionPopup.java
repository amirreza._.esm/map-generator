package generator.models.views.popup;

import generator.models.elements.data.Condition;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;


public class ConditionPopup extends WindowedPopup {

    private HBox hBox = new HBox();
    private TextField keyTf = new TextField();
    private TextField valueTf = new TextField();

    private Button createBtn = new Button("Create");

    private FormActionBtnHandler actionHandler;

    public ConditionPopup(double x, double y, String titleText, PopupMode mode) {
        super(x, y, titleText);

        switch (mode) {
            case CREATE:
                createBtn.setText("Create");
                break;
            case EDIT:
                createBtn.setText("Update");
                break;
        }

        hBox.getChildren().addAll(keyTf, valueTf, createBtn);

        createBtn.setOnMouseClicked(event -> {
            actionHandler.handle();
            this.hide();
        });

        hBox.setSpacing(3);

        setMainContent(hBox);
    }

    public Condition updateCondition(Condition condition){
        condition.setKey(keyTf.getText());
        condition.setValue(valueTf.getText());

        return condition;
    }

    public void populate(Condition condition){
        keyTf.setText(condition.getKey());
        valueTf.setText(condition.getValue());
    }


    public void setActionHandler(FormActionBtnHandler actionHandler) {
        this.actionHandler = actionHandler;
    }
}
