package generator.models.views.popup;

import generator.Controller;
import generator.models.elements.data.Action;
import generator.models.elements.data.Item;
import generator.models.views.list.ActionListView;
import generator.models.views.view.ConditionPairHolder;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;

import java.util.ArrayList;
import java.util.List;

public class ItemPopup extends WindowedPopup {

    private HBox hBox = new HBox();
    private TextField tagTF = new TextField();
    private TextArea descriptionTF = new TextArea();
    private ConditionPairHolder vcHolder = new ConditionPairHolder("Visibility conditions");

    private Button createAction = new Button("Add new action");

    private ActionListView currentActionsListview = new ActionListView();
    private Button createBtn = new Button("Create");

    private FormActionBtnHandler actionHandler;

    private List<Action> actions = new ArrayList<>();


    public ItemPopup(double x, double y, String titleText, PopupMode mode) {
        super(x, y, titleText);

        switch (mode) {
            case CREATE:
                createBtn.setText("Create");
                break;
            case EDIT:
                createBtn.setText("Update");
                break;
        }

        currentActionsListview.setList(actions);

        createAction.setOnMouseClicked(event -> {
            ActionPopup actionPopup = new ActionPopup(0, 0, "New action", PopupMode.CREATE);
            actionPopup.setPositionRelativeTo(createAction);

            actionPopup.show(this.getScene().getWindow());

            actionPopup.setActionHandler(() -> {
                Action action = actionPopup.updateAction(new Action());

                actions.add(action);
                currentActionsListview.update();

            });
        });

        createBtn.setOnMouseClicked(event -> {
            actionHandler.handle();
            this.hide();
        });

        hBox.getChildren().addAll(tagTF, descriptionTF, vcHolder,
                currentActionsListview, createAction, createBtn);

        hBox.setSpacing(3);

        setMainContent(hBox);
    }

    public Item updateItem(Item item) {
//        Item item = new Item();
        item.setName(tagTF.getText());
        item.setDescription(descriptionTF.getText());
        item.setVisibilityConditions(vcHolder.getConditions());
        item.setActions(actions);

        return item;
    }

    public void populate(Item item){
        tagTF.setText(item.getName());
        descriptionTF.setText(item.getDescription());

        actions.addAll(item.getActions());

        item.getVisibilityConditions().forEach(condition ->
                vcHolder.addConditionPair(Controller.globalData.getConditions(), condition));

        currentActionsListview.update();
    }

    public void setActionHandler(FormActionBtnHandler actionHandler) {
        this.actionHandler = actionHandler;
    }
}
