package generator.models.views.popup;

import generator.Controller;
import generator.models.elements.data.Condition;
import generator.models.elements.data.Narrative;
import generator.models.views.view.ConditionPairHolder;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;

public class NarrativePopup extends WindowedPopup {

    private HBox hBox = new HBox();

    private TextField tagTf = new TextField();

    private TextArea narrativeTf = new TextArea();
    private ConditionPairHolder vcHolder = new ConditionPairHolder("Visibility conditions");

    private Button createBtn = new Button("Create");

    private FormActionBtnHandler actionHandler;

    public NarrativePopup(double x, double y, String titleText, PopupMode mode) {
        super(x, y, titleText);

        switch (mode) {
            case CREATE:
                createBtn.setText("Create");
                break;
            case EDIT:
                createBtn.setText("Update");
                break;
        }

        createBtn.setOnMouseClicked(event -> {
            actionHandler.handle();
            this.hide();
        });

        hBox.getChildren().addAll(tagTf, narrativeTf, vcHolder, createBtn);

        hBox.setSpacing(3);

        setMainContent(hBox);
    }

    public Narrative updateNarrative(Narrative narrative) {
        narrative.setTag(tagTf.getText());
        narrative.setMassage(narrativeTf.getText());
        narrative.setConditions(vcHolder.getConditions());
        return narrative;
    }

    public void populate(Narrative narrative){
        tagTf.setText(narrative.getTag());
        narrativeTf.setText(narrative.getMassage());

        narrative.getConditions().forEach(condition ->
                vcHolder.addConditionPair(Controller.globalData.getConditions(), condition));
    }

    public void setActionHandler(FormActionBtnHandler actionHandler) {
        this.actionHandler = actionHandler;
    }

}
