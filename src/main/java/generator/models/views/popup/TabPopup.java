package generator.models.views.popup;

import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;

public class TabPopup extends WindowedPopup {

    private HBox hBox = new HBox();
    private TextField tabNameTF = new TextField();
    private Button createBtn = new Button("Create");
    public TabPopup(double x, double y) {
        super(x, y, "New tab");

        hBox.getChildren().addAll(tabNameTF, createBtn);

        hBox.setSpacing(3);

        setMainContent(hBox);
    }

    public TextField getTabNameTF() {
        return tabNameTF;
    }

    public Button getCreateBtn() {
        return createBtn;
    }
}
