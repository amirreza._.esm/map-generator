package generator.models.views.popup;

import generator.utility.Spacer;
import javafx.geometry.Insets;
import javafx.geometry.Point2D;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.stage.Popup;

public class WindowedPopup extends Popup {
    VBox container = new VBox();
    HBox topSection = new HBox();
    AnchorPane mainSection = new AnchorPane();

    Label title = new Label();
    Button closeBtn = new Button("X");

    //assistive values:
    private double deltaX;
    private double deltaY;

    public WindowedPopup(double x, double y, String titleText) {
        this.setX(x);
        this.setY(y);

        this.setAutoHide(true);
        this.setAutoFix(true);

        container.getChildren().addAll(topSection, mainSection);
        this.getContent().add(container);

        topSection.setPadding(new Insets(2, 5, 2, 5));
        topSection.setStyle("-fx-background-color: rgba(211,211,211,0.39); -fx-background-radius: 5 5 0 0;");
        topSection.setAlignment(Pos.CENTER);

        mainSection.setPadding(new Insets(5, 5, 5, 5));
        mainSection.setStyle("-fx-background-color: lightgray; -fx-background-radius: 0 0 5 5;");

        title.setText(titleText);

        HBox.setHgrow(title, Priority.ALWAYS);

        closeBtn.setOnAction(event -> {
            this.hide();
        });
        closeBtn.setPrefSize(5, 5);

        topSection.getChildren().addAll(title, Spacer.createSpacer(), closeBtn);

        container.setOnMousePressed(event -> {
            deltaX = this.getX() - event.getScreenX();
            deltaY = this.getY() - event.getScreenY();
        });
        container.setOnMouseDragged(event -> {
            this.setX(event.getScreenX() + deltaX);
            this.setY(event.getScreenY() + deltaY);
            event.consume();
        });
    }

    void setMainContent(Node node ) {
        mainSection.getChildren().add(node);
        AnchorPane.setBottomAnchor(node, 0.0);
        AnchorPane.setTopAnchor(node, 0.0);
        AnchorPane.setLeftAnchor(node, 0.0);
        AnchorPane.setRightAnchor(node, 0.0);

    }

    public void setPositionRelativeTo(Region node){
        Point2D position = node.localToScreen(new Point2D(node.getLayoutX(), node.getLayoutY() + node.getHeight() + 15));
        this.setX(position.getX());
        this.setY(position.getY());
    }

    public enum PopupMode {
        CREATE,EDIT
    }

    public interface FormActionBtnHandler{
        void handle();
    }
}
