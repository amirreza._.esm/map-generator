package generator.models.views.view;

import generator.models.data.IdTextPair;
import generator.models.elements.data.Condition;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.util.StringConverter;

import java.util.List;
import java.util.stream.Collectors;

public class ConditionPair extends HBox {
    private ChoiceBox<IdTextPair> conditionChoiceBox = new ChoiceBox<>();
    private TextField conditionValueTf = new TextField();

    public ConditionPair(List<Condition> conditionList) {
        conditionChoiceBox.setConverter(new StringConverter<IdTextPair>() {
            @Override
            public String toString(IdTextPair object) {
                return object.getText();
            }

            @Override
            public IdTextPair fromString(String string) {
                System.out.println("fromString Condition pair");
                return null;
            }
        });

        if (conditionList != null)
            conditionChoiceBox.getItems().setAll(conditionList.stream()
                    .map(condition -> new IdTextPair(condition.getConditionId(), condition.getKey()))
                    .collect(Collectors.toList()));
        this.getChildren().addAll(conditionChoiceBox, conditionValueTf);
    }

    public void setChoiceBoxValue(int id) {
        IdTextPair idTextPair = null;
        for (IdTextPair i : conditionChoiceBox.getItems())
            if (i.getId() == id)
                idTextPair = i;
        if (idTextPair != null) {
                conditionChoiceBox.setValue(idTextPair);
        }
    }

    public ChoiceBox<IdTextPair> getConditionChoiceBox() {
        return conditionChoiceBox;
    }

    public TextField getConditionValueTf() {
        return conditionValueTf;
    }

    public String getConditionValue() {
        return conditionValueTf.getText();
    }
}
