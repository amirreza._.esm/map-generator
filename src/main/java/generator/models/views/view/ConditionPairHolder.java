package generator.models.views.view;

import generator.Controller;
import generator.models.elements.data.Condition;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import java.util.ArrayList;
import java.util.List;

public class ConditionPairHolder extends HBox {

    private final Button addBtn = new Button("+");
    private final VBox listContainer = new VBox();
    private final VBox cpHolder = new VBox();

    public ConditionPairHolder(String title) {
        listContainer.getChildren().addAll(new Label(title), cpHolder);
        this.getChildren().addAll(listContainer, addBtn);

        addBtn.setOnMouseClicked(event -> {
            addConditionPair(Controller.globalData.getConditions());
        });
    }


    private void addConditionPair(List<Condition> conditionList) {
        cpHolder.getChildren().add(new ConditionPair(conditionList));
    }

    public void addConditionPair(List<Condition> conditionList, Condition condition) {
        ConditionPair conditionPair = new ConditionPair(conditionList);
        conditionPair.setChoiceBoxValue(condition.getConditionId());
        conditionPair.getConditionValueTf().setText(condition.getValue());
        cpHolder.getChildren().add(conditionPair);
    }

    public List<Condition> getConditions() {
        List<Condition> conditions = new ArrayList<>();
        for (int i = 0; i < cpHolder.getChildren().size(); i++) {
            ConditionPair pair = (ConditionPair) cpHolder.getChildren().get(i);

            Condition condition = new Condition(
                    pair.getConditionChoiceBox().getValue().getId(),
                    pair.getConditionChoiceBox().getValue().getText(),
                    pair.getConditionValue());

            conditions.add(condition);
        }
        return conditions;
    }
}
