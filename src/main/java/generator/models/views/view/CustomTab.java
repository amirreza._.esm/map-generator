package generator.models.views.view;

import generator.models.data.MapScene;
import javafx.scene.control.Tab;

public class CustomTab extends Tab {
    private MapScene mapScene;

    private ZoomPane zoomPane;

    public MapScene getMapScene() {
        return mapScene;
    }

    public CustomTab setMapScene(MapScene mapScene) {
        this.mapScene = mapScene;
        return this;
    }

    public ZoomPane getZoomPane() {
        return zoomPane;
    }

    public void setZoomPane(ZoomPane zoomPane) {
        this.zoomPane = zoomPane;
    }
}
