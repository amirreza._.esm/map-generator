package generator.models.views.view;

import generator.Controller;
import generator.models.data.IdTextPair;
import generator.models.elements.data.Narrative;
import generator.models.elements.drawable.Block;
import generator.models.views.list.ItemListView;
import generator.models.views.list.NarrativeListView;
import generator.models.views.popup.AddItemPopup;
import generator.models.views.popup.NarrativePopup;
import generator.models.views.popup.WindowedPopup;
import generator.utility.Spacer;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Point2D;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

import java.util.ArrayList;
import java.util.stream.Collectors;

public class DetailsPane extends VBox {

    private Block block;


    private TextPair idPair;
    private TextPair posXPair;
    private TextPair posYPair;
    private TextPair sizeXPair;
    private TextPair sizeYPair;

    private TabPane listsTabPane = new TabPane();

    private NarrativeListView narrativesListView = new NarrativeListView();
    private ItemListView itemsListView = new ItemListView();

    public DetailsPane() {
        setAlignment(Pos.CENTER);
        setPadding(new Insets(10, 5, 10, 5));
        setSpacing(5);

        idPair = new TextPair("Id");
        posXPair = new TextPair("posX");
        posYPair = new TextPair("posY");
        sizeXPair = new TextPair("sizeX");
        sizeYPair = new TextPair("sizeY");


        Button addTopPortalButton = new Button("Top");
        Button addRightPortalButton = new Button("Right");
        Button addBottomPortalButton = new Button("Bottom");
        Button addLeftPortalButton = new Button("Left");

        addTopPortalButton.setMaxWidth(Double.MAX_VALUE);
        addRightPortalButton.setMaxWidth(Double.MAX_VALUE);
        addBottomPortalButton.setMaxWidth(Double.MAX_VALUE);
        addLeftPortalButton.setMaxWidth(Double.MAX_VALUE);

        HBox.setHgrow(addTopPortalButton, Priority.ALWAYS);
        HBox.setHgrow(addRightPortalButton, Priority.ALWAYS);
        HBox.setHgrow(addBottomPortalButton, Priority.ALWAYS);
        HBox.setHgrow(addLeftPortalButton, Priority.ALWAYS);

        HBox peButtonContainer = new HBox(addTopPortalButton, addRightPortalButton, addBottomPortalButton, addLeftPortalButton);
        peButtonContainer.setAlignment(Pos.CENTER);
        peButtonContainer.setSpacing(5);

        Button updateButton = new Button("Update");
        updateButton.setMaxWidth(Double.MAX_VALUE);
        HBox.setHgrow(updateButton, Priority.ALWAYS);


        addTopPortalButton.setOnMouseClicked(event -> block.createPortalEnd(0));
        addRightPortalButton.setOnMouseClicked(event -> block.createPortalEnd(1));
        addBottomPortalButton.setOnMouseClicked(event -> block.createPortalEnd(2));
        addLeftPortalButton.setOnMouseClicked(event -> block.createPortalEnd(3));

        updateButton.setOnMouseClicked(event -> {
            if (block != null) {
                block.setPosition(new Point2D(Double.parseDouble(posXPair.textField.getText()), Double.parseDouble(posYPair.textField.getText())));

                if (Double.parseDouble(sizeXPair.textField.getText()) < 0) {
                    sizeXPair.setValue("0");
                }

                double sizeX = Double.parseDouble(sizeXPair.textField.getText());

                if (Double.parseDouble(sizeYPair.textField.getText()) < 0) {
                    sizeYPair.setValue("0");
                }

                double sizeY = Double.parseDouble(sizeYPair.textField.getText());

                block.setSizeInUnit(new Point2D(sizeX, sizeY));
                block.updateShape();
            } else
                Controller.showError("Update block", "No block selected.");
        });


        Button addNarrative = new Button("New narrative");

        addNarrative.setOnMouseClicked(event -> {
            if (block != null) {
                NarrativePopup newNarrativePopup = new NarrativePopup(0, 0,
                        "New narrative", WindowedPopup.PopupMode.CREATE);
                newNarrativePopup.setPositionRelativeTo(addNarrative);

                newNarrativePopup.setActionHandler(() -> {
                    block.getNarratives().add(newNarrativePopup.updateNarrative(new Narrative()));
                    newNarrativePopup.hide();
                });

                newNarrativePopup.show(this.getScene().getWindow());
            } else
                Controller.showError("New narrative", "No block selected.");
        });


        Button itemBtn = new Button("Add item");

        itemBtn.setOnMouseClicked(event -> {
            if (block != null) {
                Point2D position = itemBtn.localToScreen(new Point2D(this.getLayoutX(), itemBtn.getLayoutY() + itemBtn.getHeight() + 15));
                AddItemPopup addItemPopup = new AddItemPopup(position.getX(), position.getY(), "Add item");
                addItemPopup.show(this.getScene().getWindow());


                addItemPopup.setList(Controller.globalData.getItems());


                addItemPopup.getAddBtn().setOnMouseClicked(event1 -> {
                    addItemPopup.getItemsListView().getSelectionModel().getSelectedItems()
                            .forEach(idTextPair -> block.getItemIds().add(idTextPair.getId()));
                    addItemPopup.hide();
                    update(block);
                });
            } else
                Controller.showError("Add item", "No block selected.");
        });

        Tab narrativesTab = new Tab("Narratives");
        narrativesTab.setContent(narrativesListView);
        Tab itemsTab = new Tab("Items");
        itemsTab.setContent(itemsListView);

        listsTabPane.getTabs().addAll(narrativesTab, itemsTab);

        HBox itemNarrativeBtnContainer = new HBox(addNarrative, itemBtn);
        itemNarrativeBtnContainer.setAlignment(Pos.CENTER);
        itemNarrativeBtnContainer.setSpacing(20);

        getChildren().addAll(idPair, posXPair, posYPair, sizeXPair, sizeYPair, Spacer.createSpacer(),
                peButtonContainer,
                updateButton,
                new Separator(Orientation.HORIZONTAL),
                itemNarrativeBtnContainer,
                listsTabPane);
    }

    public Block getCurrentBlock() {
        return block;
    }

    public void update(Block block) {
        narrativesListView.getItems().clear();
        itemsListView.getItems().clear();
        if (this.block != null)
            this.block.setSelected(false);
        if (block == null) {
            this.block = null;
            idPair.setValue("");
            posXPair.setValue("");
            posYPair.setValue("");
            sizeXPair.setValue("");
            sizeYPair.setValue("");
        } else {
            this.block = block;
            this.block.setSelected(true);
            idPair.setValue(block.getBlockId() + "");
            posXPair.setValue(block.getPosition().getX() + "");
            posYPair.setValue(block.getPosition().getY() + "");
            sizeXPair.setValue(block.getSizeInUnit().getX() + "");
            sizeYPair.setValue(block.getSizeInUnit().getY() + "");
            if (block.getNarratives() != null)
                narrativesListView.setList(block.getNarratives());
            else
                narrativesListView.getItems().clear();

            if (block.getItemIds() != null)
                itemsListView.getItems().setAll(block.getItemIds().stream()
                        .map(itemId -> {
                            final IdTextPair pair = new IdTextPair(-1, "error");
                            Controller.globalData.getItems().forEach(item -> {
                                if (item.getItemId() == itemId) {
                                    pair.setId(itemId);
                                    pair.setText(item.getName());
                                }
                            });
                            return pair;
                        })
                        .collect(Collectors.toList()));
            else
                itemsListView.getItems().clear();
        }
    }

    class TextPair extends HBox {
        Label label = new Label();
        TextField textField = new TextField();

        TextPair(String labelText) {
            setAlignment(Pos.CENTER);
            setSpacing(20);
            label.setText(labelText);
            getChildren().addAll(label, Spacer.createSpacer(), textField);
        }

        public void setLabel(String label) {
            this.label.setText(label);
        }

        void setValue(String value) {
            this.textField.setText(value);
        }
    }
}
