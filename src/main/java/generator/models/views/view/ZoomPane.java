package generator.models.views.view;

import generator.Controller;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.Point2D;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.StackPane;

public class ZoomPane extends ScrollPane {

    public ZoomPane(final Group group) {
        final double SCALE_DELTA = 1.1;

//        this.setMaxHeight(Screen.getPrimary().getBounds().getHeight() * heightToParent);
//        this.setMaxWidth(Screen.getPrimary().getBounds().getWidth() * widthToParent);

        StackPane zoomPane = new StackPane();

        zoomPane.getChildren().add(group);

        final Group scrollContent = new Group(zoomPane);
        this.setContent(scrollContent);

        this.viewportBoundsProperty().addListener((observable, oldValue, newValue) -> zoomPane.setMinSize(newValue.getWidth(), newValue.getHeight()));

        this.setPrefViewportWidth(256);
        this.setPrefViewportHeight(256);

        zoomPane.setOnScroll(event -> {
            event.consume();

            if (event.getDeltaY() == 0) {
                return;
            }

            double scaleFactor = (event.getDeltaY() > 0) ? SCALE_DELTA
                    : 1 / SCALE_DELTA;

            Controller.currentMapScene.getState().zoomValue = group.getScaleX() * scaleFactor;
            // amount of scrolling in each direction in scrollContent coordinate
            // units
            Point2D scrollOffset = figureScrollOffset(scrollContent, this);

            group.setScaleX(group.getScaleX() * scaleFactor);
            group.setScaleY(group.getScaleY() * scaleFactor);

            // move viewport so that old center remains in the center after the
            // scaling

            // check to see if maximum zoom is reached
            if (scrollContent.getLayoutX() > 0)
                repositionScroller(scrollContent, this, scaleFactor, scrollOffset, this.sceneToLocal(event.getSceneX(), event.getSceneY()));

        });

        // Panning via drag....
        final ObjectProperty<Point2D> lastMouseCoordinates = new SimpleObjectProperty<>();
        scrollContent.setOnMousePressed(event -> lastMouseCoordinates.set(new Point2D(event.getX(), event.getY())));

        scrollContent.setOnMouseDragged(event -> {
            if (event.isSecondaryButtonDown()) {
                double deltaX = event.getX() - lastMouseCoordinates.get().getX();
                double extraWidth = scrollContent.getLayoutBounds().getWidth() - this.getViewportBounds().getWidth();
                double deltaH = deltaX * (this.getHmax() - this.getHmin()) / extraWidth;
                double desiredH = this.getHvalue() - deltaH;
                this.setHvalue(Math.max(0, Math.min(this.getHmax(), desiredH)));

                double deltaY = event.getY() - lastMouseCoordinates.get().getY();
                double extraHeight = scrollContent.getLayoutBounds().getHeight() - this.getViewportBounds().getHeight();
                double deltaV = deltaY * (this.getHmax() - this.getHmin()) / extraHeight;
                double desiredV = this.getVvalue() - deltaV;
                this.setVvalue(Math.max(0, Math.min(this.getVmax(), desiredV)));
            }
        });
    }

    private Point2D figureScrollOffset(Node scrollContent, ScrollPane scroller) {
        double scrollContentWidth = scrollContent.getLayoutBounds().getWidth();
//        double scrollContentWidth = scrollContent.getBoundsInLocal().getWidth() * State.zoomValue;
        double extraWidth = scrollContentWidth - scroller.getViewportBounds().getWidth();
        double hScrollProportion = (scroller.getHvalue() - scroller.getHmin()) / (scroller.getHmax() - scroller.getHmin());
        double scrollXOffset = hScrollProportion * Math.max(0, extraWidth);

        double scrollContentHeight = scrollContent.getLayoutBounds().getHeight();
//        double scrollContentHeight = scrollContent.getBoundsInLocal().getWidth() * State.zoomValue;
        double extraHeight = scrollContentHeight - scroller.getViewportBounds().getHeight();
        double vScrollProportion = (scroller.getVvalue() - scroller.getVmin()) / (scroller.getVmax() - scroller.getVmin());
        double scrollYOffset = vScrollProportion * Math.max(0, extraHeight);

        return new Point2D(scrollXOffset, scrollYOffset);
    }

    private void repositionScroller(Node scrollContent, ScrollPane scroller, double scaleFactor, Point2D scrollOffset, Point2D mousePos) {
        double scrollXOffset = scrollOffset.getX();
        double scrollYOffset = scrollOffset.getY();
        double extraWidth = scrollContent.getLayoutBounds().getWidth() - scroller.getViewportBounds().getWidth();
        if (extraWidth > 0) {
            double halfWidth = scroller.getViewportBounds().getWidth() / 2; //zoom to center
            double zoomFocusX = mousePos.getX();                            //zoom to mouse x
//            zoomFocusX = halfWidth;
            double newScrollXOffset = (scaleFactor - 1) * zoomFocusX + scaleFactor * scrollXOffset;
            scroller.setHvalue(scroller.getHmin() + newScrollXOffset * (scroller.getHmax() - scroller.getHmin()) / extraWidth);
        } else {
            scroller.setHvalue(scroller.getHmin());
        }
        double extraHeight = scrollContent.getLayoutBounds().getHeight() - scroller.getViewportBounds().getHeight();
        if (extraHeight > 0) {
            double halfHeight = scroller.getViewportBounds().getHeight() / 2;   //zoom to center
            double zoomFocusY = mousePos.getY();                                //zoom to mouse y
//            zoomFocusY = halfHeight;
            double newScrollYOffset = (scaleFactor - 1) * zoomFocusY + scaleFactor * scrollYOffset;
            scroller.setVvalue(scroller.getVmin() + newScrollYOffset * (scroller.getVmax() - scroller.getVmin()) / extraHeight);
        } else {
            scroller.setHvalue(scroller.getHmin());
        }
    }

    public void centerViewPort(){
        this.setHvalue((this.getHmax()-this.getHmin())/2);
        this.setVvalue((this.getVmax()-this.getVmin())/2);
    }
}
