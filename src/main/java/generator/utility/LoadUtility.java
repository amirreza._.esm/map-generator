package generator.utility;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import generator.Controller;
import generator.models.data.GlobalData;
import generator.models.data.MapScene;
import generator.models.data.State;
import generator.models.elements.data.Action;
import generator.models.elements.data.Condition;
import generator.models.elements.data.Item;
import generator.models.elements.data.Narrative;
import generator.models.elements.drawable.Block;
import generator.models.elements.drawable.Portal;
import generator.models.elements.drawable.PortalEnd;
import javafx.collections.FXCollections;
import javafx.geometry.Point2D;
import javafx.scene.layout.Pane;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class LoadUtility {

    public static GlobalData loadGlobalData(File file) {
        GlobalData globalData = new GlobalData();
        try {
            FileReader fr = new FileReader(file);
            JsonParser parser = new JsonParser();
            JsonObject jsonObject = parser.parse(fr).getAsJsonObject();

            //Get conditions
            JsonArray conditionsArray = jsonObject.get("conditions").getAsJsonArray();
            globalData.setConditions(FXCollections.observableArrayList(jsonArrayToConditions(conditionsArray)));

            //Get actions
            JsonArray itemArray = jsonObject.get("items").getAsJsonArray();
            globalData.setItems(FXCollections.observableArrayList(jsonArrayToItems(itemArray)));

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return globalData;
    }

    private static List<Condition> jsonArrayToConditions(JsonArray jsonArray) {
        List<Condition> conditions = new ArrayList<>();
        jsonArray.forEach(element -> {
            JsonObject object = element.getAsJsonObject();
            Condition condition = new Condition(object.get("conditionId").getAsInt());
            condition.setKey(object.get("key").getAsString());
            condition.setValue(object.get("value").getAsString());
            if (condition.getKey() != null)
                conditions.add(condition);
        });
        return conditions;
    }

    private static List<Item> jsonArrayToItems(JsonArray jsonArray) {
        List<Item> items = new ArrayList<>();
        jsonArray.forEach(jsonElement -> {
            JsonObject jsonObject = jsonElement.getAsJsonObject();
            Item item = new Item(jsonObject.get("itemId").getAsInt());
            item.setName(jsonObject.get("name").getAsString());
            item.setDescription(jsonObject.get("description").getAsString());

            item.setVisibilityConditions(jsonArrayToConditions(jsonObject.get("visibilityConditions").getAsJsonArray()));
            item.setActions(jsonArrayToActions(jsonObject.get("actions").getAsJsonArray()));

            items.add(item);
        });

        return items;
    }

    private static List<Action> jsonArrayToActions(JsonArray jsonArray) {
        List<Action> actions = new ArrayList<>();
        jsonArray.forEach(jsonElement -> {
            JsonObject jsonObject = jsonElement.getAsJsonObject();
            Action action = new Action(jsonObject.get("actionId").getAsInt());
            action.setTag(jsonObject.get("tag").getAsString());
            action.setDescription(jsonObject.get("description").getAsString());
            action.setSuccessMessage(jsonObject.get("successMessage").getAsString());
            action.setFailMessage(jsonObject.get("failMessage").getAsString());

            action.setVisibilityConditions(jsonArrayToConditions(jsonObject.get("visibilityConditions")
                    .getAsJsonArray()));
            action.setDoableConditions(jsonArrayToConditions(jsonObject.get("doableConditions")
                    .getAsJsonArray()));
            action.setChangeConditions(jsonArrayToConditions(jsonObject.get("changeConditions")
                    .getAsJsonArray()));

            actions.add(action);
        });

        return actions;
    }

    private static MapScene mapScene;

    public static MapScene loadMapScene(File file) {
        mapScene = new MapScene(Controller.CANVAS_SIZE_X, Controller.CANVAS_SIZE_Y);
        try {
            FileReader fr = new FileReader(file);
            JsonParser parser = new JsonParser();
            JsonObject jsonObject = parser.parse(fr).getAsJsonObject();

            //Get title
            mapScene.setTitle(jsonObject.get("title").getAsString());

            //Get state
            mapScene.setState(jsonToState(jsonObject));

            //Get blocks
            mapScene.getData().blocks = jsonArrayToBlocks(jsonObject);

            //Get portals
            List<Portal> portals = new ArrayList<>();
            JsonArray portalsArray = jsonObject.get("portals").getAsJsonArray();
            portalsArray.forEach(element -> {
                Portal portal = jsonToPortal(element.getAsJsonObject(), mapScene.getData().blocks);
                if (portal != null)
                    portals.add(portal);
            });
            mapScene.getData().portals = portals;


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return mapScene;
    }


    private static State jsonToState(JsonObject jsonObject) {
        State state = new State();
        JsonObject stateJson = jsonObject.get("state").getAsJsonObject();
        state.mode = stateJson.get("mode").getAsString();
        state.zoomValue = stateJson.get("zoomValue").getAsDouble();
        return state;
    }

    private static List<Block> jsonArrayToBlocks(JsonObject jsonObject) {
        List<Block> blocks = new ArrayList<>();
        JsonArray blocksArray = jsonObject.get("blocks").getAsJsonArray();
        blocksArray.forEach(element -> blocks.add(jsonToBlock(element.getAsJsonObject())));
        return blocks;
    }

    /////////

    private static Block jsonToBlock(JsonObject object) {
        int blockId = object.get("blockId").getAsInt();
        String type = object.get("type").getAsString();
        Point2D position = new Point2D(object.get("positionX").getAsDouble(), object.get("positionY").getAsDouble());
        Point2D size = new Point2D(object.get("sizeX").getAsDouble(), object.get("sizeY").getAsDouble());
        List<PortalEnd> portalEndList = new ArrayList<>();

        Block block = new Block(mapScene.getCanvas(), position, size, type);

        JsonArray portalEnds = object.get("portalEnds").getAsJsonArray();
        portalEnds.forEach(element -> portalEndList.add(jsonToPortalEnd(element.getAsJsonObject(), block)));

        block.setBlockId(blockId);
        block.setPortalEnds(portalEndList);

        List<Narrative> narrativeList = new ArrayList<>();
        JsonArray narrativesArray = object.get("narratives").getAsJsonArray();
        narrativesArray.forEach(element -> {
            JsonObject narrativeObject = element.getAsJsonObject();

            Narrative narrative = new Narrative(narrativeObject.get("narrativeId").getAsInt());
            narrative.setTag(narrativeObject.get("tag").getAsString());
            narrative.setMassage(narrativeObject.get("massage").getAsString());
            narrative.setConditions(jsonArrayToConditions(narrativeObject.get("conditions").getAsJsonArray()));

            narrativeList.add(narrative);
        });

        block.setNarratives(narrativeList);


        //Add item references
        List<Integer> itemIds = new ArrayList<>();
        JsonArray itemIdArray = object.get("itemIds").getAsJsonArray();
        itemIdArray.forEach(jsonElement -> {
            JsonObject jsonObject = jsonElement.getAsJsonObject();
            itemIds.add(jsonObject.get("itemId").getAsInt());
        });
        block.setItemIds(itemIds);

        //Create portal ends
        block.updatePortalEnds();

        //Draw block on canvas
        mapScene.getCanvas().getChildren().add(block.getShape());
        return block;
    }

    private static Portal jsonToPortal(JsonObject object, List<Block> blocks) {
        int portalId = object.get("portalId").getAsInt();
        int startX = object.get("startX").getAsInt();
        int startY = object.get("startY").getAsInt();
        int endX = object.get("endX").getAsInt();
        int endY = object.get("endY").getAsInt();
        int startPortalEndId = object.get("startPortalEnd").getAsJsonObject().get("portalEndId").getAsInt();
        int endPortalEndId = object.get("endPortalEnd").getAsJsonObject().get("portalEndId").getAsInt();

        PortalEnd start = null;
        PortalEnd end = null;
        for (Block block : blocks) {
            for (PortalEnd portalEnd : block.getPortalEnds()) {
                if (portalEnd.getPortalEndId() == startPortalEndId)
                    start = portalEnd;
                if (portalEnd.getPortalEndId() == endPortalEndId)
                    end = portalEnd;
                if (start != null && end != null)
                    break;
            }
            if (start != null && end != null)
                break;
        }
        if (start != null && end != null) {
            Portal portal = new Portal(mapScene.getCanvas(), start, end);
            portal.setPortalId(portalId);
            portal.setStartX(startX);
            portal.setStartY(startY);
            portal.setEndX(endX);
            portal.setEndY(endY);

            //Draw block on canvas
            mapScene.getCanvas().getChildren().add(portal.getShape());

            return portal;
        } else return null;
    }

    private static PortalEnd jsonToPortalEnd(JsonObject object, Block owner) {
        int portalEndId = object.get("portalEndId").getAsInt();
        int ownerBlockId = object.get("ownerBlockId").getAsInt();
        int side = object.get("side").getAsInt();
        double posX = object.get("relativePositionX").getAsDouble();
        double posY = object.get("relativePositionY").getAsDouble();

        PortalEnd portalEnd = new PortalEnd(mapScene.getCanvas(), posX, posY, ownerBlockId, side);
        portalEnd.setPortalEndId(portalEndId);

        //Draw portal end on block
        ((Pane) owner.getShape()).getChildren().add(portalEnd.getShape());
        return portalEnd;
    }
}
