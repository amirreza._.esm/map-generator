package generator.utility;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import generator.models.data.GlobalData;
import generator.models.data.MapScene;
import generator.models.data.State;
import generator.models.elements.data.Action;
import generator.models.elements.data.Condition;
import generator.models.elements.data.Item;
import generator.models.elements.drawable.Block;
import generator.models.elements.drawable.Portal;
import generator.models.elements.drawable.PortalEnd;

import java.io.BufferedWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;

public class SaveUtility {
    public static void saveGlobalData(GlobalData globalData, String saveUrl) {
        JsonObject globalDataObject = new JsonObject();

        globalDataObject.add("conditions", conditionsToJsonArray(globalData.getConditions()));

        globalDataObject.add("items", itemsToJsonArray(globalData.getItems()));

        //save
        final String content = globalDataObject.toString();
        final Path path = Paths.get(saveUrl);
        try (
                final BufferedWriter writer = Files.newBufferedWriter(path,
                        StandardCharsets.UTF_8, StandardOpenOption.CREATE)
        ) {
            writer.write(content);
            writer.flush();
        } catch (Exception e) {
            System.out.println("Save error!");
        }
    }

    private static JsonArray conditionsToJsonArray(List<Condition> conditions) {
        JsonArray conditionArray = new JsonArray();
        conditions.forEach(condition -> {
            JsonObject conditionObject = new JsonObject();
            conditionObject.addProperty("conditionId", condition.getConditionId());
            conditionObject.addProperty("key", condition.getKey());
            conditionObject.addProperty("value", condition.getValue());
            conditionArray.add(conditionObject);
        });
        return conditionArray;
    }


    private static JsonArray itemsToJsonArray(List<Item> items) {
        JsonArray itemArray = new JsonArray();
        items.forEach(item -> {
            JsonObject itemObject = new JsonObject();
            itemObject.addProperty("itemId", item.getItemId());
            itemObject.addProperty("name", item.getName());
            itemObject.addProperty("description", item.getDescription());

            itemObject.add("visibilityConditions", conditionsToJsonArray(item.getVisibilityConditions()));
            itemObject.add("actions", actionsToJsonArray(item.getActions()));

            itemArray.add(itemObject);
        });
        return itemArray;
    }

    private static JsonArray actionsToJsonArray(List<Action> actions) {
        JsonArray actionArray = new JsonArray();
        actions.forEach(action -> {
            JsonObject actionObject = new JsonObject();
            actionObject.addProperty("actionId", action.getActionId());
            actionObject.addProperty("tag", action.getTag());
            actionObject.addProperty("description", action.getDescription());
            actionObject.addProperty("successMessage", action.getSuccessMessage());
            actionObject.addProperty("failMessage", action.getFailMessage());
            actionObject.add("visibilityConditions", conditionsToJsonArray(action.getVisibilityConditions()));
            actionObject.add("doableConditions", conditionsToJsonArray(action.getDoableConditions()));
            actionObject.add("changeConditions", conditionsToJsonArray(action.getChangeConditions()));

            actionArray.add(actionObject);
        });
        return actionArray;
    }

    public static void saveMapScene(MapScene mapScene, String saveUrl) {
        JsonObject mapSceneObject = new JsonObject();

        //Save title
        mapSceneObject.addProperty("title", mapScene.getTitle());

        //Save state
        mapSceneObject.add("state", stateToJson(mapScene.getState()));

        //Save Block list
        mapSceneObject.add("blocks", blocksToJsonArray(mapScene.getData().blocks));

        //Save Portals
        mapSceneObject.add("portals", portalsToJsonArray(mapScene.getData().portals));


        //save
        final String content = mapSceneObject.toString();
        final Path path = Paths.get(saveUrl);
        try (
                final BufferedWriter writer = Files.newBufferedWriter(path,
                        StandardCharsets.UTF_8, StandardOpenOption.CREATE)
        ) {
            writer.write(content);
            writer.flush();
        } catch (Exception e) {
            System.out.println("Save error!");
        }
    }


    private static JsonObject stateToJson(State state) {
        JsonObject stateObject = new JsonObject();
        stateObject.addProperty("mode", state.mode);
        stateObject.addProperty("zoomValue", state.zoomValue);
        return stateObject;
    }

    private static JsonArray blocksToJsonArray(List<Block> blocks) {
        JsonArray blocksArray = new JsonArray();
        blocks.forEach(block -> blocksArray.add(blockToJson(block)));
        return blocksArray;
    }

    private static JsonArray portalsToJsonArray(List<Portal> portals) {
        JsonArray portalArray = new JsonArray();
        portals.forEach(portal -> portalArray.add(portalToJson(portal)));
        return portalArray;
    }

    //////////////////////////////////////

    private static JsonObject blockToJson(Block block) {
        JsonObject blockObject = new JsonObject();
        blockObject.addProperty("blockId", block.getBlockId());
        blockObject.addProperty("type", block.getType());
        blockObject.addProperty("positionX", block.getPosition().getX());
        blockObject.addProperty("positionY", block.getPosition().getY());
        blockObject.addProperty("sizeX", block.getSize().getX());
        blockObject.addProperty("sizeY", block.getSize().getY());
        JsonArray portalEndArray = new JsonArray();
        block.getPortalEnds().forEach(portalEnd -> portalEndArray.add(portalEndToJson(portalEnd)));
        blockObject.add("portalEnds", portalEndArray);

        JsonArray narrativesArray = new JsonArray();
        block.getNarratives().forEach(narrative -> {
            JsonObject narrativeObject = new JsonObject();
            narrativeObject.addProperty("narrativeId", narrative.getNarrativeId());
            narrativeObject.addProperty("tag", narrative.getTag());
            narrativeObject.addProperty("massage", narrative.getMassage());

            narrativeObject.add("conditions", conditionsToJsonArray(narrative.getConditions()));
            narrativesArray.add(narrativeObject);
        });

        blockObject.add("narratives", narrativesArray);

        JsonArray itemIdArray = new JsonArray();
        block.getItemIds().forEach(itemId -> {
            JsonObject itemIdObject = new JsonObject();
            itemIdObject.addProperty("itemId", itemId);
            itemIdArray.add(itemIdObject);
        });
        blockObject.add("itemIds", itemIdArray);

        return blockObject;
    }

    private static JsonObject portalToJson(Portal portal) {
        JsonObject portalObject = new JsonObject();
        portalObject.addProperty("portalId", portal.getPortalId());
        portalObject.addProperty("startX", portal.getStartX());
        portalObject.addProperty("startY", portal.getStartY());
        portalObject.addProperty("endX", portal.getEndX());
        portalObject.addProperty("endY", portal.getEndY());
        portalObject.add("startPortalEnd", portalEndToJson(portal.getStart()));
        portalObject.add("endPortalEnd", portalEndToJson(portal.getEnd()));

        return portalObject;
    }

    private static JsonObject portalEndToJson(PortalEnd portalEnd) {
        JsonObject portalEndObject = new JsonObject();
        portalEndObject.addProperty("portalEndId", portalEnd.getPortalEndId());
        portalEndObject.addProperty("ownerBlockId", portalEnd.getOwnerBlockId());
        portalEndObject.addProperty("side", portalEnd.getSide());
        portalEndObject.addProperty("relativePositionX", portalEnd.getPosition().getX());
        portalEndObject.addProperty("relativePositionY", portalEnd.getPosition().getY());

        return portalEndObject;
    }
}
